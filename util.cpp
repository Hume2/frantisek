#include "util.h"

namespace util {

string parse_string(FILE* f, char separator) {
  string result = "";
  char ch = fgetc(f);

  while (ch != separator && !feof(f)) {
    result += ch;
    ch = fgetc(f);
  }

  return result;
}

string parse_tag(FILE* f, int& length) {
  string result = "";
  char ch = fgetc(f);

  while (ch != '>' && ch != ':' && !feof(f)) {
    result += ch;
    ch = fgetc(f);
  }

  if (ch == ':') {
    string len = parse_string(f, '>');
    length = stoi(len);
  }

  return result;
}

void skip_to(FILE* f, char separator) {
  char ch = fgetc(f);

  while (ch != separator && !feof(f)) {
    ch = fgetc(f);
  }
}

}
