#include <algorithm>
#include <stdio.h>
#include <iostream>
#include <fstream>

#include "log.h"

#include "util.h"
#include "qso.h"
#include "config.h"

Log::Log(string filename) :
  data()
{
  FILE* f = fopen(filename.c_str(), "r");

  string eoh;
  do {
    util::skip_to(f, '<');
    eoh = util::parse_string(f, '>');
  } while (eoh != "EOH");

  while (!feof(f)) {
    QSO* qso = new QSO(f);
    if (qso->is_valid) {
      data.push_back(qso);
    } else {
      delete qso;
    }
  }

  sort(data.begin(), data.end(), QSO::is_lesser_than);
}

Log::~Log() {
  for (int i = data.size()-1; i >= 0; i--) {
    delete data[i];
  }
}

void Log::write(string filename) {
  filebuf fb;
  fb.open(filename, ios::out);
  ostream os(&fb);

  os << "<html><head><meta charset=\"UTF-8\">";
  os << "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"></head><body>";

  int i = 0;
  while (i < data.size()) {
    make_page(os, i);
  }

  os << "</body></html>";

  fb.close();
}

void Log::make_page(ostream& os, int& i) {
  os << "<table class=\"main\">\n";
  for (int y = ROWS; y; --y) {
    os << "<tr class=\"main\">";
    for (int x = COLS; x; --x) {
      os << "<td class=\"main\">";
      if (i < data.size()) {

        string call = data[i]->call;
        bool have_freq = data[i]->have_freq;
        data[i]->begin(os);
        int n = 0;

        do {
          data[i]->write(os);
          ++i;
          ++n;
        } while (i < data.size() && n < MAX_QSO && data[i]->call == call &&
                 data[i]->have_freq == have_freq);
        QSO::term(os);

      }
      os << "</td>\n";
    }
    os << "</tr>";
  }
}

