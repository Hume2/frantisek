#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <vector>
#include <string>

using namespace std;

namespace util {

string parse_string(FILE* f, char separator);
string parse_tag(FILE* f, int& length);
void skip_to(FILE* f, char separator);

}

#endif // UTIL_H

