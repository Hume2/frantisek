#include <stdio.h>
#include <iostream>

#include <ostream>
#include <string>

using namespace std;

#include "log.h"

int main(int argc, char** argv) {

  if (argc < 3) {
    cout << "Usage: ./frantisek <ADI input file> <HTML output file>\n";
    return 0;
  }

  string in = argv[1];
  string out = argv[2];
  cout << "Loading log ..." << endl;
  Log log(in);
  cout << "Saving cards ..." << endl;
  log.write(out);
  cout << "Done." << endl;

  return 0;
}

