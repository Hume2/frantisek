#ifndef QSO_H
#define QSO_H

#include <stdio.h>
#include <ostream>
#include <string>

using namespace std;

class QSO
{
  public:
    QSO(FILE* f);

    string call;
    string date;
    string time;
    string band;
    string mode;
    string rst;

    bool is_valid;
    bool have_freq;

    void begin(ostream& os);
    void write(ostream& os);
    static void term(ostream& os);

    static bool is_lesser_than(QSO* l, QSO* r);
};

#endif // QSO_H
