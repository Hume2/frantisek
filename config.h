#ifndef CONFIG_H
#define CONFIG_H

#define ROWS 6
#define COLS 3
#define MAX_QSO 6
#define ADD_FOOTER true
#define FOOTER "QSL manager Dana OK1ZKR"

#endif // CONFIG_H

