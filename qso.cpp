#include <algorithm>
#include <boost/algorithm/string/replace.hpp>

#include "qso.h"

#include "config.h"
#include "util.h"

QSO::QSO(FILE* f) :
  call(),
  date(),
  time(),
  band(),
  mode(),
  rst(),
  is_valid(false),
  have_freq(false)
{

  string tag, str;
  int len;
  util::skip_to(f, '<');

  while (true) {
    tag = util::parse_tag(f, len);

    if (feof(f)) {
      return;
    }

    if (tag == "EOR") {
      break;
    }

    str = util::parse_string(f, '<');
    str = str.substr(0, len);
    if (tag == "CALL") {
      call = str;
    } else if (tag == "BAND") {
      band = str;
      have_freq = false;
    } else if (tag == "FREQ") {
      band = str;
      have_freq = true;
    } else if (tag == "MODE") {
      mode = str;
    } else if (tag == "QSO_DATE") {
      date = str;
    } else if (tag == "TIME_ON") {
      time = str;
    } else if (tag == "RST_SENT") {
      rst = str;
    }
  }

  is_valid = !call.empty() && !band.empty() && !mode.empty() && !date.empty()
      && !time.empty() && !rst.empty();
}

bool QSO::is_lesser_than(QSO* l, QSO* r) {
  if (l->call != r->call) {
    return l->call < r->call;
  } else if (l->have_freq != r->have_freq) {
    return r->have_freq;
  } else if (l->date != r->date) {
    return l->date < r->date;
  } else {
    return l->time < r->time;
  }
}

void QSO::begin(ostream& os) {
  string call_ = boost::replace_all_copy(call, "0", "&Oslash;");


  os << "<table class=\"qso\">";
  os << "<tr class=\"qso\"><th colspan=2 class=\"to_radio\">To Radio:</th><th colspan=3 class=\"call\">" << call_ << "</th></tr>";
  os << "<tr class=\"qso\"><th class=\"legend\">Date</th><th class=\"legend\">Time</th><th class=\"legend\">";
  if (have_freq) {
    os << "Frequency";
  } else {
    os << "Band";
  }
  os << "</th><th class=\"legend\">Mode</th><th class=\"legend\">RST</th></tr>";
}

void QSO::write(ostream& os) {
  string band_ = band;
  std::transform(band_.begin(), band_.end(), band_.begin(), ::tolower);

  string date_ = date;
  date_.insert(6, "-");
  date_.insert(4, "-");

  string time_ = time.substr(0, 4);
  time_.insert(2, ":");

  os << "<tr class=\"qso\"><td class=\"date\">" << date_ << "</td><td class=\"time\">" << time_ << "</td><td class=\"band\">" << band_ << "</td><td class=\"mode\">" << mode << "</td><td class=\"rst\">" << rst << "</td></tr>";
}

void QSO::term(ostream& os) {
  if (ADD_FOOTER) {
    os << "<tr><td colspan=5 class=\"footer\">" << FOOTER << "</td></tr>";
  }
  os << "</table>";
}
